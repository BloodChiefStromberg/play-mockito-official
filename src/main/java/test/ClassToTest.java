package test;

public class ClassToTest {
    private final MyDatabase database;

    public ClassToTest(MyDatabase db) {
        this.database = db;
    }

    // simple bs query method
    public boolean query(String query) {
        database.query(query);
        return true;
    }
}
