package test;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

// This is one easy way to init the annotated mocks and spies and maybe do some other scaffolding...[1]
//@RunWith(MockitoJUnitRunner.class)
public class MockitoTest  {

    @Mock
    MyDatabase databaseMock; // This mocks the database -- you can call any method you want on it, and it will fake
                             // return stuff unless we tell it to do diff with when(...).thenReturn(...)

    // [1]...this is another way I have only seen recommended once...[2]
    //@Rule MockitoRule mockitoRule = MockitoJUnit.rule();

    // [2]...and this is another way.
    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testQuery()  {
        ClassToTest t  = new ClassToTest(databaseMock); // use the mock to make our class
        boolean check = t.query("* from t"); // query our class with a non-mocked method
        assertTrue(check); // make sure it returned true
        verify(databaseMock).query("* from t"); // make sure the query method was actually called on the mocked obj
    }

    @Test
    public void unique_id() {
        MyDatabase test = mock(MyDatabase.class);

        when(test.getUniqueId()).thenReturn(43);

        assertEquals(test.getUniqueId(), 43);
    }

    @Test
    public void whenSpyingOnList_thenCorrect() {
        List<String> list = new ArrayList<>();
        List<String> spyList = spy(list); // note I don't have to do Mockito.spy because I statically imported it

        spyList.add("one");
        spyList.add("two");

        verify(spyList).add("two");
        verify(spyList).add("one"); // tried these in reverse order, works

        assertEquals(2, spyList.size());
    }

    @Test
    public void whenStubASpy_thenStubbed() {
        List<String> list = new ArrayList<>();
        List<String> spy = spy(list);

        assertEquals(0, spy.size());

        doReturn(100).when(spy).size(); // this is stubbing the spy obj
        assertEquals(100, spy.size());
    }
}